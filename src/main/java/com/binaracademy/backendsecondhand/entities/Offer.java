package com.binaracademy.backendsecondhand.entities;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.binaracademy.backendsecondhand.utils.StatusOffer;
import com.binaracademy.backendsecondhand.utils.StatusTransaction;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Offer")
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Offer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "offer_id")
	private Integer id;

	@Column(name = "priceoffer")
	private Integer priceOffer;

	@Column(name = "dateoffer")
	private ZonedDateTime dateOffer;

	@Column(name = "statusoffer")
	@Enumerated(EnumType.STRING)
	private StatusOffer statusOffer;

	@Column(name = "succeed")
	@Enumerated(EnumType.STRING)
	private StatusTransaction statusTransaction;

	@JoinColumn(name = "product_id", referencedColumnName = "product_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private Product productId;

	@JoinColumn(name = "buyer_id", referencedColumnName = "user_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private User buyerId;

	public Offer(Integer priceOffer, ZonedDateTime dateOffer, StatusOffer statusOffer, StatusTransaction statusTransaction,
			Product productId, User buyerId) {
		super();
		this.priceOffer = priceOffer;
		this.dateOffer = dateOffer;
		this.statusOffer = StatusOffer.PROCESSING;
		this.statusTransaction = StatusTransaction.PROCESSING;
		this.productId = productId;
		this.buyerId = buyerId;
	}
}
