package com.binaracademy.backendsecondhand.entities;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Product")
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "product_id")
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "price")
	private Integer price;

	@Column(name = "datecreated")
	private ZonedDateTime dateCreated;

	@Column(name = "dateupdated")
	private ZonedDateTime dateUpdated;

	@Column(name = "location")
	private String location;

	@Column(name = "published")
	private boolean published;

	@JoinColumn(name = "seller_id", referencedColumnName = "user_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private User sellerId;

	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
	private List<ProductPicture> pictures;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "ProductCategory", joinColumns = @JoinColumn(name = "product_id"), inverseJoinColumns = @JoinColumn(name = "category_id"))
	private List<Category> categories;

	public Product(String name, String description, Integer price, ZonedDateTime dateCreated, ZonedDateTime dateUpdated, String location,
			boolean published, User sellerId, List<ProductPicture> pictures, List<Category> categories) {
		super();
		this.name = name;
		this.description = description;
		this.price = price;
		this.dateCreated = dateCreated;
		this.dateUpdated = dateUpdated;
		this.location = location;
		this.published = published;
		this.sellerId = sellerId;
		this.pictures = pictures;
		this.categories = categories;
	}

	public Product(String name, String description, Integer price, ZonedDateTime dateCreated, ZonedDateTime dateUpdated, String location,
			boolean published, User sellerId, List<Category> categories) {
		super();
		this.name = name;
		this.description = description;
		this.price = price;
		this.dateCreated = dateCreated;
		this.dateUpdated = dateUpdated;
		this.location = location;
		this.published = published;
		this.sellerId = sellerId;
		this.categories = categories;
	}

	public Product(String name, String description, Integer price, ZonedDateTime dateUpdated, String location, boolean published,
			User sellerId, List<Category> categories) {
		super();
		this.name = name;
		this.description = description;
		this.price = price;
		this.dateUpdated = dateUpdated;
		this.location = location;
		this.published = published;
		this.sellerId = sellerId;
		this.categories = categories;
	}
	
}
