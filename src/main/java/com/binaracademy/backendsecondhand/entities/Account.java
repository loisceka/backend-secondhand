package com.binaracademy.backendsecondhand.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.binaracademy.backendsecondhand.entities.location.City;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Account")
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "account_id")
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "address")
	private String address;

	@Column(name = "phone")
	private String phone;
	
	@JoinColumn(name ="city")
	@ManyToOne
	private City city;

	@JsonIgnore
	@Column(name = "profilepicture")
	@Lob
	@Type(type = "org.hibernate.type.ImageType")
	private byte[] profilePicture;
	
	@JsonIgnore
	@OneToOne(mappedBy = "accountId")
	private User userId;
	
	public Account(String name) {
		super();
		this.name = name;
	}

	public Account(String name, String address, String phone, City city, byte[] profilePicture, User userId) {
		super();
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.city = city;
		this.profilePicture = profilePicture;
		this.userId = userId;
	}
}
