package com.binaracademy.backendsecondhand.entities;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Category")
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "category_id")
	private Integer id;

	@Column(name = "name")
	private String name;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "categories", cascade = CascadeType.ALL)
    private List<Product> products;
	
	public Category(String name, List<Product> products) {
		super();
		this.name = name;
		this.products = products;
	}

	public Category(String name) {
		super();
		this.name = name;
	}
}
