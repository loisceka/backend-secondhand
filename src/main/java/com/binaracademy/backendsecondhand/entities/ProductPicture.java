package com.binaracademy.backendsecondhand.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ProductPicture")
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class ProductPicture {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Basic(optional = false)
	@Column(name = "picture_id")
	private String id;

	@Column(name = "name")
	private String pictureName;

	@Column(name = "type")
	private String type;

	@Column(name = "data")
	@Lob
	@Type(type = "org.hibernate.type.ImageType")
	private byte[] data;

	@ManyToOne
	@JoinColumn(name = "product_id", nullable = false)
	private Product product;

	public ProductPicture(String pictureName, String type, byte[] data, Product product) {
		super();
		this.pictureName = pictureName;
		this.type = type;
		this.data = data;
		this.product = product;
	}
}
