package com.binaracademy.backendsecondhand.entities;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Wishlist")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Wishlist {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "wishlist_id")
	private Integer id;

	@Column(name = "wishlistdate")
	private ZonedDateTime wishlistDate;
	
	@JoinColumn(name = "product_id", referencedColumnName = "product_id")
	@ManyToOne(fetch = FetchType.LAZY)
    private Product productId;
	
	@JoinColumn(name = "buyer_id", referencedColumnName = "user_id")
	@ManyToOne(fetch = FetchType.LAZY)
    private User buyerId;

	public Wishlist(ZonedDateTime wishlistDate, Product productId, User buyerId) {
		super();
		this.wishlistDate = wishlistDate;
		this.productId = productId;
		this.buyerId = buyerId;
	}
}
