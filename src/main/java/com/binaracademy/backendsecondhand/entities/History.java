package com.binaracademy.backendsecondhand.entities;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "History")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class History {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "history_id")
	private Integer id;

	@Column(name = "datecreated")
	private ZonedDateTime dateCreated;

	@OneToOne
	@JoinColumn(name = "offer_id", referencedColumnName = "offer_id")
	private Offer offerId;

	@OneToOne
	@JoinColumn(name = "product_id", referencedColumnName = "product_id")
	private Product productId;

	public History(ZonedDateTime dateCreated, Offer offerId, Product productId) {
		super();
		this.dateCreated = dateCreated;
		this.offerId = offerId;
		this.productId = productId;
	}
}
