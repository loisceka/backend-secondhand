package com.binaracademy.backendsecondhand.entities.location;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "province")
@AllArgsConstructor
@NoArgsConstructor
public class Province {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "province_id")
	private Integer id;

	@Column(name = "name")
	private String name;
	
	@JsonIgnore
	@OneToMany(mappedBy ="province")
	private List<City> cities;

	public Province(String name) {
		super();
		this.name = name;
	}
	
}
