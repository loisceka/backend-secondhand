package com.binaracademy.backendsecondhand.entities.location;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "city")
@AllArgsConstructor
@NoArgsConstructor
public class City {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "city_id")
	private Integer id;

	@Column(name = "name")
	private String name;
	
	@ManyToOne
	@JoinColumn(name ="province_id", nullable=false)
	private Province province;

	public City(String name, Province province) {
		super();
		this.name = name;
		this.province = province;
	}
}
