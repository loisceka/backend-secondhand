package com.binaracademy.backendsecondhand.services;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binaracademy.backendsecondhand.dtos.ProductPictureResponse;
import com.binaracademy.backendsecondhand.dtos.WishlistRequest;
import com.binaracademy.backendsecondhand.dtos.WishlistResponse;
import com.binaracademy.backendsecondhand.entities.ProductPicture;
import com.binaracademy.backendsecondhand.entities.Wishlist;
import com.binaracademy.backendsecondhand.repositories.WishlistRepository;
import com.binaracademy.backendsecondhand.utils.DateTimeUtils;

@Service
public class WishlistService {

	@Autowired
	private WishlistRepository wishlistRepository;
	@Autowired
	private ProductService productService;
	@Autowired
	private UserService userService;
	@Autowired
	private DateTimeUtils dateTimeUtils;

	public List<WishlistResponse> getAllByBuyerId(Integer id) {
		List<Wishlist> listW = wishlistRepository.findByBuyerId(id);
		List<WishlistResponse> listWr = new ArrayList<>();
		for (Wishlist wishlist : listW) {
			WishlistResponse wr = new WishlistResponse();
			wr = mapToWishlistResponse(wishlist);
			listWr.add(wr);
		}
		return listWr;
	}

	public Wishlist getByIdService(Integer id) {
		return wishlistRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Wishlist not found"));
	}

	public WishlistResponse create(WishlistRequest request) {
		ZonedDateTime zdt = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Asia/Jakarta"));
		Wishlist wishlist = new Wishlist();
		wishlist.setWishlistDate(zdt);
		wishlist.setProductId(productService.getByIdService(request.getProductId()));
		wishlist.setBuyerId(userService.getById(request.getBuyerId()));
		wishlistRepository.save(wishlist);
		WishlistResponse wr = mapToWishlistResponse(wishlist);
		return wr;
	}


	public WishlistResponse delete(Integer id) {
		Wishlist wishlist = getByIdService(id);
		wishlistRepository.deleteById(id);
		
		WishlistResponse wr = mapToWishlistResponse(wishlist);
		return wr;
	}
	
	public WishlistResponse mapToWishlistResponse(Wishlist wishlist) {
		
		List<ProductPictureResponse> ppResponse = new ArrayList<>();
		for (ProductPicture pp : wishlist.getProductId().getPictures()) {
			ProductPictureResponse pprs = new ProductPictureResponse();
			pprs = productService.mapToPictureResponse(pp);
			ppResponse.add(pprs);
		}
		
		WishlistResponse wr = new WishlistResponse(
				wishlist.getId(),
				dateTimeUtils.mapToDateString(wishlist.getWishlistDate()),
				productService.mapToProductResponse(wishlist.getProductId(), ppResponse),
				wishlist.getBuyerId()
				);
		
		return wr;
	}
}
