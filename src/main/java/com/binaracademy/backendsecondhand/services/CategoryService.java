package com.binaracademy.backendsecondhand.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binaracademy.backendsecondhand.entities.Category;
import com.binaracademy.backendsecondhand.repositories.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	public List<Category> getAll() {
		return categoryRepository.findAll();
	}

	public Category getById(Integer id) {
		return categoryRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Category not found"));
	}

	public Category create(Category category) {
		if (category.getId() != null) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Category already exist");
		}
		return categoryRepository.save(category);
	}

	public Category update(Integer id, Category category) {
		Category oldCategoryData = getById(id);
		category.setId(oldCategoryData.getId());

		return categoryRepository.save(category);
	}

	public Category delete(Integer id) {
		Category category = getById(id);

		categoryRepository.deleteById(id);
		return category;
	}
}
