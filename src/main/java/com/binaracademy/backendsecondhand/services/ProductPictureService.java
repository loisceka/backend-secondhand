package com.binaracademy.backendsecondhand.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import com.binaracademy.backendsecondhand.entities.ProductPicture;
import com.binaracademy.backendsecondhand.repositories.ProductPictureRepository;

@Service
public class ProductPictureService {
	@Autowired
	private ProductPictureRepository productPictureRepository;
	
	public ProductPicture getById(String id) {
		return productPictureRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product Picture not found"));
	}
	
	public List<ProductPicture> getAll() {
		return productPictureRepository.findAll();
	}
	
	// Get all by product id
	public List<ProductPicture> getAllById(Integer id) {
		return productPictureRepository.findByProductId(id);
	}
}
