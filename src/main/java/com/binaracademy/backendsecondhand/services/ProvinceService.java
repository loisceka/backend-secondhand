package com.binaracademy.backendsecondhand.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binaracademy.backendsecondhand.entities.location.Province;
import com.binaracademy.backendsecondhand.repositories.ProvinceRepository;

@Service
public class ProvinceService {

	@Autowired
	private ProvinceRepository provinceRepository;
	
	public List<Province> getAll() {
		return provinceRepository.findAll();
	}

	public Province getById(Integer id) {
		return provinceRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Province not found"));
	}

	public Province create(Province province) {
		if (province.getId() != null) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Province already exist");
		}
		return provinceRepository.save(province);
	}

	public Province update(Integer id, Province province) {
		Province oldProvinceData = getById(id);
		province.setId(oldProvinceData.getId());

		return provinceRepository.save(province);
	}

	public Province delete(Integer id) {
		Province province = getById(id);

		provinceRepository.deleteById(id);
		return province;
	}
}
