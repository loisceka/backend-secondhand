package com.binaracademy.backendsecondhand.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.binaracademy.backendsecondhand.dtos.AccountResponse;
import com.binaracademy.backendsecondhand.entities.Account;
import com.binaracademy.backendsecondhand.entities.location.City;
import com.binaracademy.backendsecondhand.repositories.AccountRepository;

@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private CityService cityService;

	public Account getById(Integer id) {
		return accountRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Account not found"));
	}

	public Account update(Integer id, String name, 
			String address, 
			String phone,
			Integer cityId,
			MultipartFile document) throws IOException{
		Account oldAccountData = getById(id);
		City city = cityService.getById(cityId);
		Account newAcc = new Account(
				name,
				address,
				phone,
				city,
				document.getBytes(),
				oldAccountData.getUserId()
				);
		newAcc.setId(oldAccountData.getId());

		return accountRepository.save(newAcc);
	}
	
	public AccountResponse mapToResponse(Account acc) {
		String ids = acc.getId().toString();
		String downloadURL = ServletUriComponentsBuilder.fromCurrentContextPath().path("/account/image/")
				.path(ids).toUriString();
		AccountResponse accResponse = new AccountResponse();
		accResponse.setId(acc.getId());
		accResponse.setName(acc.getName());
		accResponse.setPhone(acc.getPhone());
		accResponse.setCity(acc.getCity());
		accResponse.setAddress(acc.getAddress());
		accResponse.setImageUrl(downloadURL);

		return accResponse;
	}
}
