package com.binaracademy.backendsecondhand.services;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.binaracademy.backendsecondhand.dtos.ProductPictureResponse;
import com.binaracademy.backendsecondhand.dtos.ProductResponse;
import com.binaracademy.backendsecondhand.entities.Category;
import com.binaracademy.backendsecondhand.entities.Offer;
import com.binaracademy.backendsecondhand.entities.Product;
import com.binaracademy.backendsecondhand.entities.ProductPicture;
import com.binaracademy.backendsecondhand.entities.User;
import com.binaracademy.backendsecondhand.repositories.OfferRepository;
import com.binaracademy.backendsecondhand.repositories.ProductPictureRepository;
import com.binaracademy.backendsecondhand.repositories.ProductRepository;
import com.binaracademy.backendsecondhand.utils.DateTimeUtils;
import com.binaracademy.backendsecondhand.utils.StatusOffer;

@Service
@Transactional
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private ProductPictureRepository productPictureRepository;
	@Autowired
	private UserService userService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private DateTimeUtils dateTimeUtils;
	@Autowired
	private OfferRepository offerRepository;
	
	// All product
	public List<ProductResponse> getAllPublishedAndPageable(Integer pageNo, Integer pageSize){
		Pageable paging = PageRequest.of(pageNo, pageSize);
		System.out.println(paging.toString());
		Page<Product> pageProduct = productRepository.getAllByPublished(true, paging);
		System.out.println(pageProduct.getNumberOfElements());
		List<ProductResponse> prs = new ArrayList<>();
		for (Product prod : pageProduct) {
			List<ProductPictureResponse> ppr = productPictureRepository.findByProductId(prod.getId())
					.stream().map(this::mapToPictureResponse)
					.collect(Collectors.toList());
			ProductResponse pr = mapToProductResponse(prod, ppr);
			prs.add(pr);
		}
		return prs;
	}
	
	// All Published = True Product
	public List<ProductResponse> getAllPublishedProduct() {
		List<Product> products = productRepository.findByPublished(true);
		List<ProductResponse> prs = new ArrayList<>();
		for (Product prod : products) {
			List<ProductPictureResponse> ppr = productPictureRepository.findByProductId(prod.getId())
					.stream().map(this::mapToPictureResponse)
					.collect(Collectors.toList());
			ProductResponse pr = mapToProductResponse(prod, ppr);
			prs.add(pr);
		}
		return prs;
	}
	
	public List<ProductResponse> getAllProductByCategory(Integer categoryId) {
		List<Product> products = productRepository.findByCategories(categoryId);
		List<ProductResponse> prs = new ArrayList<>();
		for (Product prod : products) {
			List<ProductPictureResponse> ppr = productPictureRepository.findByProductId(prod.getId())
					.stream().map(this::mapToPictureResponse)
					.collect(Collectors.toList());
			ProductResponse pr = mapToProductResponse(prod, ppr);
			prs.add(pr);
		}
		return prs;
	}
	
	public List<ProductResponse> getAllProductSeller(Integer sellerId) {
		List<Product> products = productRepository.findBySellerId(sellerId, true);
		List<ProductResponse> prs = new ArrayList<>();
		for (Product prod : products) {
			List<ProductPictureResponse> ppr = productPictureRepository.findByProductId(prod.getId())
					.stream().map(this::mapToPictureResponse)
					.collect(Collectors.toList());
			ProductResponse pr = mapToProductResponse(prod, ppr);
			prs.add(pr);
		}
		return prs;
	}
	
	public List<ProductResponse> getAllProductKeyword(String keyword){
		List<Product> products = productRepository.findByKeyword(keyword);
		List<ProductResponse> prs = new ArrayList<>();
		for (Product prod : products) {
			List<ProductPictureResponse> ppr = productPictureRepository.findByProductId(prod.getId())
					.stream().map(this::mapToPictureResponse)
					.collect(Collectors.toList());
			ProductResponse pr = mapToProductResponse(prod, ppr);
			prs.add(pr);
		}
		return prs;
	}
	
	// Get by id
	public ProductResponse getById(Integer productId) {
		Product pro = getByIdService(productId);
		List<ProductPictureResponse> ppr = productPictureRepository.findByProductId(pro.getId())
				.stream().map(this::mapToPictureResponse)
				.collect(Collectors.toList());
		ProductResponse pr = mapToProductResponse(pro, ppr);
		return pr;
	}
	
	// get product buyert
	public ProductResponse getByIdBuyer(Integer productId, Integer buyerId) {
		Offer offer = offerRepository.findByBuyerId(productId, buyerId);
		Product pro = getByIdService(productId);
		List<ProductPictureResponse> ppr = productPictureRepository.findByProductId(pro.getId())
				.stream().map(this::mapToPictureResponse)
				.collect(Collectors.toList());
		ProductResponse pr = mapToProductResponse(pro, ppr);
		if (offer != null) {
			if (offer.getStatusOffer() == StatusOffer.DECLINED) {
				pr.setOffered(false);
			} else {
				pr.setOffered(true);
			}
		} else {
			pr.setOffered(false);
		}
		return pr;
	}
	
	
	
	
	public Product getByIdService(Integer id) {
		return  productRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found"));
	}

	// Create or Add Product
	public ProductResponse addProduct(List<MultipartFile> documents, String name, 
			String description, Integer price, List<Integer> categoryId, Integer sellerId) throws IOException {
		ZonedDateTime zdt = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Asia/Jakarta"));
		boolean gambar = documents.isEmpty();
		if (gambar == true) {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Picture must not be null");
		}
		if (productRepository.findBySellerId(sellerId, true).size() >= 4) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Product already max(4) !");
		}
		if (documents.size() > 4) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Pictures must not be more than 4!");
		}
		User newSeller = userService.getById(sellerId);
		if (newSeller == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Seller ID Not Found !");
		}
		List<Category> cats = new ArrayList<>();
		for(Integer i : categoryId) {
			Category c = categoryService.getById(i);
			cats.add(c);
		}
		if (newSeller.getAccountId().getCity() == null) {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "City must be filled first !");
		}
		String lokasi = newSeller.getAccountId().getCity().getName();
		Product pro = new Product(
				name, 
				description, 
				price, 
				zdt, //Product datecreated
				zdt, //Product dateupdated
				lokasi, 
				true, // Published status
				newSeller, // Seller
				cats);
		productRepository.save(pro);
		System.out.println(pro.getName());
		List<ProductPicture> pics = new ArrayList<>();
		for (MultipartFile docs : documents) {
			ProductPicture pic = new ProductPicture();
			pic.setPictureName(docs.getOriginalFilename());
			pic.setType(docs.getContentType());
			pic.setData(docs.getBytes());
			pic.setProduct(pro);
			pics.add(pic);
		}
		productPictureRepository.saveAll(pics);
		List<ProductPictureResponse> ppResponse = new ArrayList<>();
		for (ProductPicture pp : pics) {
			ProductPictureResponse pprs = new ProductPictureResponse();
			pprs = mapToPictureResponse(pp);
			ppResponse.add(pprs);
		}
		return mapToProductResponse(pro, ppResponse);
	}
	
	// Edit product(basic)
	public ProductResponse editProduct(Integer id, List<MultipartFile> documents, String name, 
			String description, Integer price,
			List<Integer> categoryId, Integer sellerId) throws IOException {
		ZonedDateTime zdt = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Asia/Jakarta"));
		boolean gambar = documents.isEmpty();
		if (gambar == true) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Picture must not be null !"); 
		}
		if (documents.size() > 4) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Pictures must not be more than 4!");
		}
		Product oldProductData = getByIdService(id);
		User newSeller = userService.getById(sellerId);
		for	(Integer i = 0; i < productPictureRepository.findByProductId(oldProductData.getId()).size(); i++) {
			productPictureRepository.deletePictureByProductId(oldProductData.getId());
		}
		List<Category> cats = new ArrayList<>();
		for(Integer i : categoryId) {
			Category c = categoryService.getById(i);
			cats.add(c);
		}
		Product pro = new Product(name, 
				description, 
				price, 
				oldProductData.getDateCreated(),
				zdt, 
				newSeller.getAccountId().getCity().getName(), 
				true, // Published status
				newSeller, // Seller ID
				cats);
		pro.setId(oldProductData.getId());
		productRepository.save(pro);
		
		List<ProductPicture> pics = new ArrayList<>();
		for (MultipartFile docs : documents) {
			ProductPicture pic = new ProductPicture();
			pic.setPictureName(docs.getOriginalFilename());
			pic.setType(docs.getContentType());
			pic.setData(docs.getBytes());
			pic.setProduct(pro);
			pics.add(pic);
		}
		productPictureRepository.saveAll(pics);
		List<ProductPictureResponse> ppResponse = new ArrayList<>();
		for (ProductPicture pp : pics) {
			ProductPictureResponse pprs = new ProductPictureResponse();
			pprs = mapToPictureResponse(pp);
			ppResponse.add(pprs);
		}
		return mapToProductResponse(pro, ppResponse);
	}
	
	
	// soft delete
	public ProductResponse deleteProduct(Integer id) {
		Product product = getByIdService(id);
		product.setPublished(false);
		productRepository.save(product);
		List<ProductPictureResponse> ppResponse = new ArrayList<>();
		for (ProductPicture pp : product.getPictures()) {
			ProductPictureResponse pprs = new ProductPictureResponse();
			pprs = mapToPictureResponse(pp);
			ppResponse.add(pprs);
		}
		return mapToProductResponse(product, ppResponse);
	}
	
	public ProductPictureResponse mapToPictureResponse(ProductPicture prod) {
		String downloadURL = ServletUriComponentsBuilder.fromCurrentContextPath().path("/product/picture/file/")
				.path(prod.getId()).toUriString();
		ProductPictureResponse ppResponse = new ProductPictureResponse();
		ppResponse.setId(prod.getId());
		ppResponse.setName(prod.getPictureName());
		ppResponse.setContentType(prod.getType());
		ppResponse.setUrl(downloadURL);

		return ppResponse;
	}

	public ProductResponse mapToProductResponse(Product pro, List<ProductPictureResponse> ppResponse) {
		ProductResponse productResponse = new ProductResponse(
				pro.getId(),
				pro.getName(),
				pro.getDescription(),
				pro.getPrice(),
				pro.getLocation(),
				dateTimeUtils.mapToDateString(pro.getDateCreated()),
				dateTimeUtils.mapToDateString(pro.getDateUpdated()),
				pro.isPublished(),
				userService.mapToUserResponse(pro.getSellerId()),
				pro.getCategories(),
				ppResponse
				);
		return productResponse;
	}
}
