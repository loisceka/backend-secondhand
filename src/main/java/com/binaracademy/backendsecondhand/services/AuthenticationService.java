package com.binaracademy.backendsecondhand.services;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binaracademy.backendsecondhand.dtos.LoginRequest;
import com.binaracademy.backendsecondhand.dtos.LoginResponse;
import com.binaracademy.backendsecondhand.dtos.RegisterRequest;
import com.binaracademy.backendsecondhand.dtos.RegisterResponse;
import com.binaracademy.backendsecondhand.entities.Account;
import com.binaracademy.backendsecondhand.entities.Role;
import com.binaracademy.backendsecondhand.entities.User;
import com.binaracademy.backendsecondhand.repositories.AccountRepository;
import com.binaracademy.backendsecondhand.repositories.RoleRepository;
import com.binaracademy.backendsecondhand.repositories.UserRepository;

@Service
public class AuthenticationService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private PasswordEncoder encoder;
	
	public RegisterResponse register(RegisterRequest request) {
		if (userRepository.findByEmail(request.getEmail()) != null) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Email Has Already Exist");
		}
		System.out.println(userRepository.findByEmail(request.getEmail()));
		Set<Role> roles = new HashSet<>();
		// Give default role
		roles.add(roleRepository.findByName("SELLER"));
		roles.add(roleRepository.findByName("BUYER"));
		
		Account account = new Account(
				request.getName());
		accountRepository.save(account);
		
		User user = new User(
				null,  
				request.getEmail(), 
				encoder.encode(request.getPassword()),
				roles,
				account);

		 return new RegisterResponse().generate(userRepository.save(user));
	}
	
	
	
	public LoginResponse login(LoginRequest request) {
		User user = userRepository.findByEmail(request.getEmail());

		if (user == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found!");
		}
		if (!encoder.matches(request.getPassword(), user.getPassword())) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Wrong password!");
		}

		return new LoginResponse(createLoginToken(request.getEmail(), request.getPassword()),
				generateAuthorities(user.getRoles()), user.getId());
	}
	
	public String createLoginToken(String identity, String password) {
		String auth = identity + ":" + password;
		byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.US_ASCII));

		return new String(encodedAuth);
	}


	private Set<String> generateAuthorities(Set<Role> roles) {
		Set<String> authorities = new HashSet<>();

		for (Role role : roles) {
			authorities.add("ROLE_" + role.getName().toUpperCase());
		}

		return authorities;
	}


}
