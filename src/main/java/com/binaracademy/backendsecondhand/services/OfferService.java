package com.binaracademy.backendsecondhand.services;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binaracademy.backendsecondhand.dtos.OfferRequest;
import com.binaracademy.backendsecondhand.dtos.OfferResponse;
import com.binaracademy.backendsecondhand.dtos.ProductPictureResponse;
import com.binaracademy.backendsecondhand.dtos.ProductResponse;
import com.binaracademy.backendsecondhand.entities.History;
import com.binaracademy.backendsecondhand.entities.Offer;
import com.binaracademy.backendsecondhand.entities.Product;
import com.binaracademy.backendsecondhand.entities.ProductPicture;
import com.binaracademy.backendsecondhand.repositories.HistoryRepository;
import com.binaracademy.backendsecondhand.repositories.OfferRepository;
import com.binaracademy.backendsecondhand.utils.DateTimeUtils;
import com.binaracademy.backendsecondhand.utils.StatusOffer;
import com.binaracademy.backendsecondhand.utils.StatusTransaction;

@Service
public class OfferService {

	@Autowired
	private OfferRepository offerRepository;
	@Autowired
	private HistoryRepository historyRepository;
	@Autowired
	private UserService userService;
	@Autowired
	private ProductService productService;
	@Autowired
	private DateTimeUtils dateTimeUtils;

	public List<OfferResponse> getAllForSeller(Integer id) {
		List<Offer> offers = offerRepository.findBySellerId(id, true);
		List<OfferResponse> ors = new ArrayList<>();
		for	(Offer offer : offers) {
			OfferResponse or = new OfferResponse();
			or = mapToOfferResponse(offer);
			ors.add(or);
		}
		return ors;
	}
	
	public List<ProductResponse> getAllProductSeller(Integer id) {
		List<Product> products = new ArrayList<>();
		List<ProductResponse> prs = new ArrayList<>();
		Product pro = new Product();
		List<Integer> productsId = offerRepository.findProductId(id, true);
		if (productsId.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There's no product");
		}
		for (Integer i = 0; i < productsId.size(); i++) {
			pro = productService.getByIdService(productsId.get(i));
			products.add(pro);
		}
		List<ProductPictureResponse> ppResponse = new ArrayList<>();
		for (ProductPicture pp : pro.getPictures()) {
			ProductPictureResponse pprs = new ProductPictureResponse();
			pprs = productService.mapToPictureResponse(pp);
			ppResponse.add(pprs);
		}
		for (Product prod : products) {
			ProductResponse pr = productService.mapToProductResponse(prod, ppResponse);
			prs.add(pr);
		}
		return prs;
	}
	
	public List<OfferResponse> getAllOfferForProduct(Integer id) {
		List<Offer> offers = offerRepository.findByProductId(id);
		List<OfferResponse> ors = new ArrayList<>();
		for	(Offer offer : offers) {
			OfferResponse or = new OfferResponse();
			or = mapToOfferResponse(offer);
			ors.add(or);
		}
		return ors;
	}
	
	public Offer getOfferProductAndBuyer(Integer productId, Integer buyerId) {
		Offer offer = offerRepository.findByBuyerId(productId, buyerId);
		return offer;
	}
	
	public List<OfferResponse> getAllForBuyer(Integer id) {
		List<Offer> offers = offerRepository.findByBuyerIds(id, true);
		List<OfferResponse> ors = new ArrayList<>();
		for	(Offer offer : offers) {
			OfferResponse or = new OfferResponse();
			or = mapToOfferResponse(offer);
			ors.add(or);
		}
		return ors;
	}

	public Offer getByIdService(Integer id) {
		return offerRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Offer not found"));
	}
	
	public OfferResponse getById(Integer id) {
		Offer offer = getByIdService(id);
		OfferResponse or = mapToOfferResponse(offer);
		return or;
	}

	public OfferResponse create(OfferRequest request) {
		ZonedDateTime zdt = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Asia/Jakarta"));
		Offer offer = new Offer();
		offer.setPriceOffer(request.getPriceOffer());
		offer.setBuyerId(userService.getById(request.getBuyerId()));
		offer.setProductId(productService.getByIdService(request.getProductId()));
		offer.setDateOffer(zdt);
		offer.setStatusOffer(StatusOffer.PROCESSING);
		offer.setStatusTransaction(StatusTransaction.PROCESSING);
		offerRepository.save(offer);
		OfferResponse or = mapToOfferResponse(offer);
		or.getProduct().setOffered(true);
		return or;
	}

	public OfferResponse update(Integer id, OfferRequest request) {
		ZonedDateTime zdt = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Asia/Jakarta"));
		Offer oldOfferData = getByIdService(id);
		Offer newOfferData = new Offer();
		newOfferData.setDateOffer(zdt);
		newOfferData.setPriceOffer(request.getPriceOffer());
		newOfferData.setProductId(productService.getByIdService(request.getProductId()));
		newOfferData.setStatusOffer(StatusOffer.PROCESSING);
		newOfferData.setStatusTransaction(StatusTransaction.PROCESSING);
		newOfferData.setBuyerId(userService.getById(request.getBuyerId()));
		newOfferData.setId(oldOfferData.getId());
		offerRepository.save(newOfferData);
		
		OfferResponse or = mapToOfferResponse(newOfferData);
		or.getProduct().setOffered(true);
		return or;
	}
	
	// Status -> Accepted
	public OfferResponse acceptOffer(Integer id) {
		Offer oldOfferData = getByIdService(id);
		oldOfferData.setStatusOffer(StatusOffer.ACCEPTED);

		offerRepository.save(oldOfferData);
		OfferResponse or = mapToOfferResponse(oldOfferData);
		return or;
	}

	// Status -> Declined
	public OfferResponse declineOffer(Integer id) {
		Offer oldOfferData = getByIdService(id);
		oldOfferData.setStatusOffer(StatusOffer.DECLINED);
		oldOfferData.setStatusTransaction(StatusTransaction.FAILED);

		offerRepository.save(oldOfferData);
		OfferResponse or = mapToOfferResponse(oldOfferData);
		return or;
	}

	// Transaction Status -> Succed -> History created
	public OfferResponse succeedTransaction(Integer id){
		ZonedDateTime zdt = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Asia/Jakarta"));
		Offer oldOfferData = getByIdService(id);
		if(oldOfferData.getStatusOffer() != StatusOffer.ACCEPTED) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Offer is not accepted yet !");
		}
		oldOfferData.setStatusTransaction(StatusTransaction.SUCCEED);
		History history = new History(
					zdt,
					getByIdService(id),
					oldOfferData.getProductId()
				);
		historyRepository.save(history);
		offerRepository.save(oldOfferData);
		productService.deleteProduct(oldOfferData.getProductId().getId());
		OfferResponse or = mapToOfferResponse(oldOfferData);
		return or;
	}

	// Transaction Status -> Failed
	public OfferResponse failedTransaction(Integer id) {
		ZonedDateTime zdt = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Asia/Jakarta"));
		Offer oldOfferData = getByIdService(id);
		if(oldOfferData.getStatusOffer() != StatusOffer.ACCEPTED) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Offer is not accepted yet !");
		}
		oldOfferData.setStatusTransaction(StatusTransaction.FAILED);
		History history = new History(
				zdt,
				getByIdService(id),
				oldOfferData.getProductId()
			);
		historyRepository.save(history);
		offerRepository.save(oldOfferData);
		OfferResponse or = mapToOfferResponse(oldOfferData);
		return or;
	}

	public OfferResponse delete(Integer id) {
		Offer offer = getByIdService(id);

		offerRepository.deleteById(id);
		
		OfferResponse or = mapToOfferResponse(offer);
		return or;
	}
	
	public OfferResponse mapToOfferResponse(Offer offer) {
		List<ProductPictureResponse> ppResponse = new ArrayList<>();
		for (ProductPicture pp : offer.getProductId().getPictures()) {
			ProductPictureResponse pprs = new ProductPictureResponse();
			pprs = productService.mapToPictureResponse(pp);
			ppResponse.add(pprs);
		}
		
		OfferResponse or = new OfferResponse(
				offer.getId(),
				offer.getPriceOffer(),
				productService.mapToProductResponse(offer.getProductId(), ppResponse),
				userService.mapToUserResponse(offer.getBuyerId()),
				dateTimeUtils.mapToDateString(offer.getDateOffer()),
				offer.getStatusOffer(),
				offer.getStatusTransaction()
				);
		return or;
	}
}
