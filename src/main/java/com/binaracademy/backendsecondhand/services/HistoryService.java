package com.binaracademy.backendsecondhand.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binaracademy.backendsecondhand.dtos.HistoryResponse;
import com.binaracademy.backendsecondhand.dtos.OfferResponse;
import com.binaracademy.backendsecondhand.entities.History;
import com.binaracademy.backendsecondhand.repositories.HistoryRepository;
import com.binaracademy.backendsecondhand.utils.DateTimeUtils;

@Service
public class HistoryService {

	@Autowired
	private HistoryRepository historyRepository;
	@Autowired
	private OfferService offerService;
	@Autowired
	private DateTimeUtils dateTimeUtils;

	public List<HistoryResponse> getAllForSeller(Integer id) {
		List<History> histories = historyRepository.findAllBySellerId(id);
		List<HistoryResponse> hrs = new ArrayList<>();
		for	(History history : histories) {
			HistoryResponse hr = new HistoryResponse();
			hr = mapToHistoryResponse(history);
			hrs.add(hr);
		}
		return hrs;
	}
	
	public List<HistoryResponse> getAllForBuyer(Integer id) {
		List<History> histories = historyRepository.findAllByBuyerId(id);
		List<HistoryResponse> hrs = new ArrayList<>();
		for	(History history : histories) {
			HistoryResponse hr = new HistoryResponse();
			hr = mapToHistoryResponse(history);
			hrs.add(hr);
		}
		return hrs;
	}

	public History getByIdService(Integer id) {
		return historyRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "History not found"));
	}
	
	public HistoryResponse getById(Integer id) {
		History history = getByIdService(id);
		HistoryResponse hr = mapToHistoryResponse(history);
		return hr;
	}

	public HistoryResponse delete(Integer id) {
		History history = getByIdService(id);
		historyRepository.deleteById(id);
		
		HistoryResponse hr = mapToHistoryResponse(history);
		return hr;
	}
	
	public HistoryResponse mapToHistoryResponse(History history) {
		OfferResponse or = offerService.mapToOfferResponse(history.getOfferId());
		
		HistoryResponse hr = new HistoryResponse(
				history.getId(),
				dateTimeUtils.mapToDateString(history.getDateCreated()),
				or,
				or.getProduct()
				);
		return hr;
	}
}
