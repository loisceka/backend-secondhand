package com.binaracademy.backendsecondhand.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binaracademy.backendsecondhand.entities.location.City;
import com.binaracademy.backendsecondhand.repositories.CityRepository;

@Service
public class CityService {

	@Autowired
	private CityRepository cityRepository;
	
	public List<City> getAll() {
		return cityRepository.findAll();
	}

	public City getById(Integer id) {
		return cityRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "City not found"));
	}

	public City create(City city) {
		if (city.getId() != null) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "City already exist");
		}
		return cityRepository.save(city);
	}

	public City update(Integer id, City city) {
		City oldCityData = getById(id);
		city.setId(oldCityData.getId());

		return cityRepository.save(city);
	}

	public City delete(Integer id) {
		City city = getById(id);

		cityRepository.deleteById(id);
		return city;
	}
}
