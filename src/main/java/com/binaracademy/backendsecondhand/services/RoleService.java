package com.binaracademy.backendsecondhand.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binaracademy.backendsecondhand.entities.Role;
import com.binaracademy.backendsecondhand.repositories.RoleRepository;

@Service
public class RoleService {

	@Autowired
	private RoleRepository roleRepository;

	public List<Role> getAll() {
		return roleRepository.findAll();
	}

	public Role getById(Integer id) {
		return roleRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Role not found"));
	}

	public Role create(Role role) {
		if (role.getId() != null) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Role already exist");
		}
		return roleRepository.save(role);
	}

	public Role update(Integer id, Role role) {
		Role oldRoleData = getById(id);
		role.setId(oldRoleData.getId());

		return roleRepository.save(role);
	}

	public Role delete(Integer id) {
		Role role = getById(id);

		roleRepository.deleteById(id);
		return role;
	}
}
