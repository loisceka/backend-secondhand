package com.binaracademy.backendsecondhand.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binaracademy.backendsecondhand.dtos.UserResponse;
import com.binaracademy.backendsecondhand.entities.User;
import com.binaracademy.backendsecondhand.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AccountService accountService;

	public User getById(Integer id) {
		return userRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
	}

	public User update(Integer id, User user) {
		User oldUserData = getById(id);
		user.setId(oldUserData.getId());
		return userRepository.save(user);
	}
	
	public UserResponse mapToUserResponse (User user) {
		UserResponse ur = new UserResponse();
		ur.setId(user.getId());
		ur.setEmail(user.getEmail());
		ur.setAccountId(accountService.mapToResponse(user.getAccountId()));
		return ur;
	}
}
