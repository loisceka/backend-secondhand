package com.binaracademy.backendsecondhand;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendSecondhandApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendSecondhandApplication.class, args);
		System.out.println("Application is running....");
	}

}
