package com.binaracademy.backendsecondhand.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.binaracademy.backendsecondhand.entities.ProductPicture;

@Repository
public interface ProductPictureRepository extends JpaRepository<ProductPicture, String>{

	
	@Query(value="SELECT * FROM product_picture WHERE product_id = ?1", nativeQuery = true)
    public List<ProductPicture> findByProductId(Integer id);
	
	@Modifying
	@Query(value = "DELETE FROM product_picture WHERE product_id = ?1", nativeQuery = true)
	public void deletePictureByProductId(Integer id);
}
