package com.binaracademy.backendsecondhand.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.binaracademy.backendsecondhand.entities.Offer;
import com.binaracademy.backendsecondhand.entities.Product;

@Repository
public interface OfferRepository extends JpaRepository<Offer, Integer>{
	
	// Query untuk list offer yang dikirimkan oleh buyer
	@Query(value = "SELECT * FROM offer o LEFT JOIN product p "
			+ "ON o.product_id = p.product_id WHERE o.buyer_id = ?1 AND p.published = ?2", 
			nativeQuery = true)
	List<Offer> findByBuyerIds(Integer buyerId, boolean published);
	
	// Query untuk list offer yang diterima oleh seller
	@Query(value = "SELECT * FROM offer o LEFT JOIN product p "
			+ "ON o.product_id = p.product_id WHERE p.seller_id = ?1 AND p.published =?2", 
			nativeQuery = true)
	List<Offer> findBySellerId(Integer sellerId, boolean published);
	
	@Query(value = "SELECT o.product_id FROM offer o INNER JOIN product p "
			+ "ON o.product_id = p.product_id WHERE p.seller_id = ?1 AND p.published =?2 GROUP BY o.product_id",
			nativeQuery= true)
	List<Integer> findProductId(Integer sellerId, boolean published);
	
	@Query(value = "SELECT * FROM offer o LEFT JOIN product p "
			+ "ON o.product_id = p.product_id WHERE o.product_id = ?1", 
			nativeQuery = true)
	List<Offer> findByProductId(Integer productId);
	
	@Query(value = "SELECT * FROM offer o WHERE o.product_id = ?1 AND o.buyer_id = ?2 ORDER BY o.offer_id DESC LIMIT 1",
			nativeQuery = true)
	Offer findByBuyerId(Integer productId, Integer buyerId);
}
