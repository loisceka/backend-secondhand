package com.binaracademy.backendsecondhand.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.binaracademy.backendsecondhand.entities.Wishlist;

@Repository
public interface WishlistRepository extends JpaRepository<Wishlist, Integer> {
	
	@Query(value = "SELECT * FROM wishlist w WHERE w.buyer_id = ?1", 
			nativeQuery = true)
	List<Wishlist> findByBuyerId(Integer buyerId);
}
