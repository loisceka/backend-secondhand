package com.binaracademy.backendsecondhand.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.binaracademy.backendsecondhand.entities.History;

@Repository
public interface HistoryRepository extends JpaRepository<History, Integer> {

	@Query(value = "SELECT * FROM history h LEFT JOIN product p "
			+ "ON h.product_id = p.product_id WHERE p.seller_id = ?1", 
			nativeQuery = true)
	List<History> findAllBySellerId(Integer id);
	
	@Query(value = "SELECT * FROM history h LEFT JOIN offer o "
			+ "ON h.offer_id = o.offer_id WHERE o.buyer_id = ?1", 
			nativeQuery = true)
	List<History> findAllByBuyerId(Integer id);
}
