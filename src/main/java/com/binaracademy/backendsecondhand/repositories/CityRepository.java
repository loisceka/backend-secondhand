package com.binaracademy.backendsecondhand.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.binaracademy.backendsecondhand.entities.location.City;

@Repository
public interface CityRepository extends JpaRepository<City, Integer>{

}
