package com.binaracademy.backendsecondhand.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.binaracademy.backendsecondhand.entities.location.Province;

@Repository
public interface ProvinceRepository extends JpaRepository<Province, Integer> {

}
