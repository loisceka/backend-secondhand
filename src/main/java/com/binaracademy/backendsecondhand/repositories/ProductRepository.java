package com.binaracademy.backendsecondhand.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.binaracademy.backendsecondhand.entities.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{
	
	List<Product> findByPublished(boolean published);
	
	@Query(value = "SELECT * FROM product p WHERE p.name LIKE LOWER(concat('%',:keyword,'%'))"
			+ " OR p.description LIKE LOWER(concat('%',:keyword,'%'))",
            nativeQuery = true)
    List<Product> findByKeyword(@Param("keyword") String keyword);
	
	@Query(value = "SELECT * FROM product p LEFT JOIN product_category pc "
			+ "ON p.product_id = pc.product_id WHERE pc.category_id = ?1", 
			nativeQuery = true)
	List<Product> findByCategories(Integer categoryId);
	
	@Modifying
	@Query(value = "DELETE product p, product_category pc, product_picture pp FROM product INNER JOIN product_category "
			+ "ON pc.product_id = p.product_id INNER JOIN product_picture ON pp.product_id = p.producT_id WHERE p.product_id = ?1", nativeQuery = true)
	public void deleteProductCategoryAndPicture(Integer produtId);
	
	@Query(value = "SELECT * FROM product p WHERE p.seller_id = ?1 AND p.published = ?2",
			nativeQuery = true)
	List<Product> findBySellerId(Integer sellerId, boolean published);
	
	
	//Testing pagination 1
	Page<Product> getAllByPublished(boolean published, Pageable pageable);
}
