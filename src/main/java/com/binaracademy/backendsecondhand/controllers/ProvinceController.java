package com.binaracademy.backendsecondhand.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binaracademy.backendsecondhand.entities.location.Province;
import com.binaracademy.backendsecondhand.services.ProvinceService;

@RestController
@RequestMapping("/province")
public class ProvinceController {

	@Autowired
	private ProvinceService provinceService;
	
	@GetMapping
	public ResponseEntity<List<Province>> getAll() {
		return new ResponseEntity<>(provinceService.getAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Province> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(provinceService.getById(id), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@PostMapping
	public ResponseEntity<Province> create(@RequestBody Province province) {
		return new ResponseEntity<>(provinceService.create(province), HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@PutMapping("/{id}")
	public ResponseEntity<Province> update(@PathVariable("id") Integer id, @RequestBody Province province) {
		return new ResponseEntity<>(provinceService.update(id, province), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Province> delete(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(provinceService.delete(id), HttpStatus.OK);
	}
}
