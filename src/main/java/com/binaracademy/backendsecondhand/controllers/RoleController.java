package com.binaracademy.backendsecondhand.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binaracademy.backendsecondhand.entities.Role;
import com.binaracademy.backendsecondhand.services.RoleService;

@RestController
@RequestMapping("/role")
public class RoleController {

	@Autowired
	private RoleService roleService;

	@GetMapping
	public ResponseEntity<List<Role>> getAll() {
		return new ResponseEntity<>(roleService.getAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Role> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(roleService.getById(id), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@PostMapping
	public ResponseEntity<Role> create(@RequestBody Role role) {
		return new ResponseEntity<>(roleService.create(role), HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@PutMapping("/{id}")
	public ResponseEntity<Role> update(@PathVariable("id") Integer id, @RequestBody Role role) {
		return new ResponseEntity<>(roleService.update(id, role), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Role> delete(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(roleService.delete(id), HttpStatus.OK);
	}
}
