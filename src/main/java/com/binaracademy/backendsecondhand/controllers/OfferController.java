package com.binaracademy.backendsecondhand.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.binaracademy.backendsecondhand.dtos.OfferRequest;
import com.binaracademy.backendsecondhand.dtos.OfferResponse;
import com.binaracademy.backendsecondhand.dtos.ProductResponse;
import com.binaracademy.backendsecondhand.services.OfferService;

@RestController
@RequestMapping("/offer")
public class OfferController {
	
	@Autowired
	private OfferService offerService;
	
	@PreAuthorize("hasRole('ROLE_SELLER')")
	@GetMapping("/seller/{id}")
	public ResponseEntity<List<OfferResponse>> getAllForSeller(@PathVariable("id") Integer id){
		return new ResponseEntity<>(offerService.getAllForSeller(id), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_BUYER')")
	@GetMapping("/buyer/{id}")
	public ResponseEntity<List<OfferResponse>> getAllForBuyer(@PathVariable("id") Integer id){
		return new ResponseEntity<>(offerService.getAllForBuyer(id), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@GetMapping("/{id}")
	public ResponseEntity<OfferResponse> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(offerService.getById(id), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_SELLER')")
	@GetMapping("/seller")
	public ResponseEntity<List<ProductResponse>> getProductOfferSeller(@RequestParam("id") Integer id) {
		return new ResponseEntity<>(offerService.getAllProductSeller(id), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@GetMapping("/product")
	public ResponseEntity<List<OfferResponse>> getOfferForProduct(@RequestParam("id") Integer id) {
		return new ResponseEntity<>(offerService.getAllOfferForProduct(id), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_BUYER')")
	@PostMapping("/create")
	public ResponseEntity<OfferResponse> createOffer(@RequestBody OfferRequest request) {
		return new ResponseEntity<>(offerService.create(request), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_BUYER')")
	@PutMapping("/{id}")
	// Edit offer for buyer
	public ResponseEntity<OfferResponse> updateOffer(@PathVariable("id") Integer id, 
			@RequestBody OfferRequest offer){
		return new ResponseEntity<>(offerService.update(id, offer), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_SELLER')")
	@PutMapping("/accept/{id}")
	// Accept offer
	public ResponseEntity<OfferResponse> acceptOffer(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(offerService.acceptOffer(id), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_SELLER')")
	@PutMapping("/decline/{id}")
	// Decline offer
	public ResponseEntity<OfferResponse> declineOffer(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(offerService.declineOffer(id), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_SELLER')")
	@PutMapping("/succeed/{id}")
	// Transaction Succeed
	public ResponseEntity<OfferResponse> succeedOffer(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(offerService.succeedTransaction(id), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_SELLER')")
	@PutMapping("/failed/{id}")
	// Transaction Failed
	public ResponseEntity<OfferResponse> failedOffer(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(offerService.failedTransaction(id), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@DeleteMapping("/{id}")
	public ResponseEntity<OfferResponse> deleteOffer(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(offerService.delete(id), HttpStatus.OK);
	}
}
