package com.binaracademy.backendsecondhand.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binaracademy.backendsecondhand.dtos.LoginRequest;
import com.binaracademy.backendsecondhand.dtos.LoginResponse;
import com.binaracademy.backendsecondhand.dtos.RegisterRequest;
import com.binaracademy.backendsecondhand.dtos.RegisterResponse;
import com.binaracademy.backendsecondhand.services.AuthenticationService;

@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	private AuthenticationService authenticationService;

	@PostMapping("/register")
	public ResponseEntity<RegisterResponse> register(@RequestBody RegisterRequest request) {
		return new ResponseEntity<>(authenticationService.register(request), HttpStatus.OK);
	}

	@PostMapping("/login")
	public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest request) {
		return new ResponseEntity<>(authenticationService.login(request), HttpStatus.OK);
	}

}
