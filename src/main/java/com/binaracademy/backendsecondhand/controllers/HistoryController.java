package com.binaracademy.backendsecondhand.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binaracademy.backendsecondhand.dtos.HistoryResponse;
import com.binaracademy.backendsecondhand.services.HistoryService;

@RestController
@RequestMapping("/history")
public class HistoryController {

	@Autowired
	private HistoryService historyService;
	
	
	// Get by id Buyer
	@PreAuthorize("hasRole('ROLE_BUYER')")
	@GetMapping("/buyer/{id}")
	public ResponseEntity<List<HistoryResponse>> getAllForBuyer(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(historyService.getAllForBuyer(id), HttpStatus.OK);
	}
	
	// Get by id Seller
	@PreAuthorize("hasRole('ROLE_SELLER')")
	@GetMapping("/seller/{id}")
	public ResponseEntity<List<HistoryResponse>> getAllForSeller(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(historyService.getAllForSeller(id), HttpStatus.OK);
	}	
	
	// Get by id
	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@GetMapping("/{id}")
	public ResponseEntity<HistoryResponse> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(historyService.getById(id), HttpStatus.OK);
	}
}
