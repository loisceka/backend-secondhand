package com.binaracademy.backendsecondhand.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.binaracademy.backendsecondhand.dtos.ProductPictureResponse;
import com.binaracademy.backendsecondhand.entities.ProductPicture;
import com.binaracademy.backendsecondhand.services.ProductPictureService;

@RestController
@RequestMapping("/product/picture")
public class ProductPictureController {

	@Autowired
	private ProductPictureService productPictureService;

	@GetMapping("/file/{id}")
	public ResponseEntity<byte[]> getFile(@PathVariable String id) {
		ProductPicture productPicture = productPictureService.getById(id);

		if (productPicture == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION,
						"attachment; filename=\"" + productPicture.getPictureName() + "\"")
				.contentType(MediaType.valueOf(productPicture.getType())).body(productPicture.getData());
	}
	
	@GetMapping
    public List<ProductPictureResponse> list() {
        return productPictureService.getAll()
                          .stream()
                          .map(this::mapToPictureResponse)
                          .collect(Collectors.toList());
    }
	
	@GetMapping("/{id}")
	public List<ProductPictureResponse> listByProduct(@PathVariable Integer id) {
		return productPictureService.getAllById(id)
				.stream()
				.map(this::mapToPictureResponse)
				.collect(Collectors.toList());
	}

	public ProductPictureResponse mapToPictureResponse(ProductPicture prod) {
		String downloadURL = ServletUriComponentsBuilder.fromCurrentContextPath().path("/product/picture/file/")
				.path(prod.getId()).toUriString();
		ProductPictureResponse ppResponse = new ProductPictureResponse();
		ppResponse.setId(prod.getId());
		ppResponse.setName(prod.getPictureName());
		ppResponse.setContentType(prod.getType());
		ppResponse.setUrl(downloadURL);

		return ppResponse;
	}
}
