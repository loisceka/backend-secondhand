package com.binaracademy.backendsecondhand.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.binaracademy.backendsecondhand.dtos.AccountRequest;
import com.binaracademy.backendsecondhand.dtos.AccountResponse;
import com.binaracademy.backendsecondhand.entities.Account;
import com.binaracademy.backendsecondhand.services.AccountService;

@RestController
@RequestMapping("/account")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@GetMapping("/{id}")
	public ResponseEntity<AccountResponse> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(accountService.mapToResponse(accountService.getById(id))
				, HttpStatus.OK);
	}
	
	// Edit / Update Account Table
	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<AccountResponse> addAccount(
			@PathVariable("id") Integer id,
			@ModelAttribute("Account") AccountRequest request) throws IOException{
		return new ResponseEntity<>(accountService.mapToResponse(accountService.update(id, request.getName(), request.getAddress(),
				request.getPhone(), request.getCityId(), request.getDocument())), HttpStatus.OK);
	}
	
	@GetMapping("/image/{id}")
	public ResponseEntity<byte[]> getFile(@PathVariable Integer id) {
		Account account = accountService.getById(id);

		if (account == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION,
						"attachment; filename=\"" + "profilepicture"+ "\"")
				.contentType(MediaType.valueOf("image/jpeg")).body(account.getProfilePicture());
	}
	
}
