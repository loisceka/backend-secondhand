package com.binaracademy.backendsecondhand.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binaracademy.backendsecondhand.entities.Category;
import com.binaracademy.backendsecondhand.services.CategoryService;

@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@GetMapping
	public ResponseEntity<List<Category>> getAll() {
		return new ResponseEntity<>(categoryService.getAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Category> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(categoryService.getById(id), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@PostMapping
	public ResponseEntity<Category> create(@RequestBody Category category) {
		return new ResponseEntity<>(categoryService.create(category), HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@PutMapping("/{id}")
	public ResponseEntity<Category> update(@PathVariable("id") Integer id, @RequestBody Category category) {
		return new ResponseEntity<>(categoryService.update(id, category), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Category> delete(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(categoryService.delete(id), HttpStatus.OK);
	}
}
