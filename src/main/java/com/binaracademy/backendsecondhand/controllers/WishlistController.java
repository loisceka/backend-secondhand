package com.binaracademy.backendsecondhand.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binaracademy.backendsecondhand.dtos.WishlistRequest;
import com.binaracademy.backendsecondhand.dtos.WishlistResponse;
import com.binaracademy.backendsecondhand.services.WishlistService;

@RestController
@RequestMapping("/wishlist")
public class WishlistController {

	@Autowired
	private WishlistService wishlistService;

	@PreAuthorize("hasRole('ROLE_BUYER')")
	@GetMapping("/user/{id}")
	public ResponseEntity<List<WishlistResponse>> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(wishlistService.getAllByBuyerId(id), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_BUYER')")
	@PostMapping
	public ResponseEntity<WishlistResponse> create(@RequestBody WishlistRequest request) {
		return new ResponseEntity<>(wishlistService.create(request), HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('ROLE_BUYER')")
	@DeleteMapping("/{id}")
	public ResponseEntity<WishlistResponse> delete(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(wishlistService.delete(id), HttpStatus.OK);
	}
}
