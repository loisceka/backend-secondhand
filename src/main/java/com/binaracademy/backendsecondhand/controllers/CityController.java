package com.binaracademy.backendsecondhand.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binaracademy.backendsecondhand.entities.location.City;
import com.binaracademy.backendsecondhand.services.CityService;

@RestController
@RequestMapping("/city")
public class CityController {

	@Autowired
	private CityService cityService;

	@GetMapping
	public ResponseEntity<List<City>> getAll() {
		return new ResponseEntity<>(cityService.getAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<City> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(cityService.getById(id), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@PostMapping
	public ResponseEntity<City> create(@RequestBody City city) {
		return new ResponseEntity<>(cityService.create(city), HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@PutMapping("/{id}")
	public ResponseEntity<City> update(@PathVariable("id") Integer id, @RequestBody City city) {
		return new ResponseEntity<>(cityService.update(id, city), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@DeleteMapping("/{id}")
	public ResponseEntity<City> delete(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(cityService.delete(id), HttpStatus.OK);
	}
}
