package com.binaracademy.backendsecondhand.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.binaracademy.backendsecondhand.dtos.ProductRequest;
import com.binaracademy.backendsecondhand.dtos.ProductResponse;
import com.binaracademy.backendsecondhand.services.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	// Create or Add Product
	@PreAuthorize("hasRole('ROLE_SELLER')")
	@RequestMapping(value = "/addProduct", method = RequestMethod.POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<ProductResponse> addProduct(
			@ModelAttribute("Product") ProductRequest request) throws IOException{
		return new ResponseEntity<>(productService.addProduct(request.getDocuments(), request.getName(), request.getDescription(),
				request.getPrice(), request.getCategories(), request.getSellerId()), HttpStatus.OK);
	}
	
	//Edit or update Product
	@PreAuthorize("hasRole('ROLE_SELLER')")
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.PUT, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<ProductResponse> editProduct(
			@PathVariable("id") Integer id,
			@ModelAttribute("Product") ProductRequest request) throws IOException{
		return new ResponseEntity<>(productService.editProduct(id, request.getDocuments(), request.getName(), request.getDescription(),
				request.getPrice(), request.getCategories(), request.getSellerId()), HttpStatus.CREATED);
	}
	
	// All published product
	@GetMapping("/all")
	public ResponseEntity<List<ProductResponse>> getAll() {
		return new ResponseEntity<>(productService.getAllPublishedProduct(), HttpStatus.OK);
	}
	
	@GetMapping("/category")
	public ResponseEntity<List<ProductResponse>> getAllByCategory(@RequestParam("id") Integer id) {
		return new ResponseEntity<>(productService.getAllProductByCategory(id), HttpStatus.OK);
	}
	
	@GetMapping("/seller/{id}")
	public ResponseEntity<List<ProductResponse>> getAllBySellerId(@PathVariable("id") Integer id){
		return new ResponseEntity<>(productService.getAllProductSeller(id), HttpStatus.OK);
	}
	
	@GetMapping("/newProduct")
	public ResponseEntity<List<ProductResponse>> getAllProductPage(
			@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize){
		return new ResponseEntity<>(productService.getAllPublishedAndPageable(pageNo, pageSize), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProductResponse> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(productService.getById(id), HttpStatus.OK);
	}
	
	@GetMapping("/offerbuyer")
	public ResponseEntity<ProductResponse> getByIdBuyer(@RequestParam("productid") Integer productId,
			@RequestParam("userid") Integer buyerId) {
		return new ResponseEntity<>(productService.getByIdBuyer(productId, buyerId), HttpStatus.OK);
	}
	
	// Delete
	@PreAuthorize("hasRole('ROLE_SELLER')")
	@DeleteMapping("/{id}")
	public ResponseEntity<ProductResponse> delete(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(productService.deleteProduct(id), HttpStatus.OK);
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<ProductResponse>> searchProduct(@RequestParam("keyword") String keyword) {
		return new ResponseEntity<>(productService.getAllProductKeyword(keyword), HttpStatus.OK);
	}
}
