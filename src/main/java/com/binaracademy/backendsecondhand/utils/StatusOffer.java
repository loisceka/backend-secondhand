package com.binaracademy.backendsecondhand.utils;

public enum StatusOffer {
	PROCESSING,
	ACCEPTED,
	DECLINED
}
