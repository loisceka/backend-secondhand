package com.binaracademy.backendsecondhand.utils;

public enum StatusTransaction {
	PROCESSING,
	SUCCEED,
	FAILED
}
