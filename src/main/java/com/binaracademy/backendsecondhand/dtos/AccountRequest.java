package com.binaracademy.backendsecondhand.dtos;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountRequest {

	private String name;
	private String address;
	private String phone;
	private Integer cityId;
	private MultipartFile document; 
}
