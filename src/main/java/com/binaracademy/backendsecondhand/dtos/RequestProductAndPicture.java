package com.binaracademy.backendsecondhand.dtos;

import java.sql.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.binaracademy.backendsecondhand.entities.Category;
import com.binaracademy.backendsecondhand.entities.ProductPicture;
import com.binaracademy.backendsecondhand.entities.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestProductAndPicture {

	private String name;
	private String description;
	private Integer price;
	private String location;
	private Integer sellerId;
	private List<Category> categories;
	private List<MultipartFile> pictures;

}
