package com.binaracademy.backendsecondhand.dtos;

import com.binaracademy.backendsecondhand.entities.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterResponse {

	private String email;

	public RegisterResponse generate(User user) {
		return new RegisterResponse(user.getEmail());
	}
}
