package com.binaracademy.backendsecondhand.dtos;

import java.sql.Date;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HistoryResponse {

	private Integer id;
	private String dateCreated;
	private OfferResponse offer;
	private ProductResponse product;
}
