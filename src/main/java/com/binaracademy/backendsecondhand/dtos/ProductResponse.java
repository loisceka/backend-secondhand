package com.binaracademy.backendsecondhand.dtos;

import java.time.LocalDateTime;
import java.util.List;

import com.binaracademy.backendsecondhand.entities.Category;
import com.binaracademy.backendsecondhand.entities.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {
	
	private Integer id;
	private String name;
	private String description;
	private Integer price;
	private String location;
	private String dateCreated;
	private String dateUpdated;
	private boolean published;
	private boolean offered;
	private UserResponse seller;
	private List<Category> categories;
	private List<ProductPictureResponse> productPictures;
	
	public ProductResponse(Integer id, String name, String description, Integer price, String location,
			String dateCreated, String dateUpdated, boolean published, UserResponse seller, List<Category> categories,
			List<ProductPictureResponse> productPictures) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.location = location;
		this.dateCreated = dateCreated;
		this.dateUpdated = dateUpdated;
		this.published = published;
		this.seller = seller;
		this.categories = categories;
		this.productPictures = productPictures;
	}
}
