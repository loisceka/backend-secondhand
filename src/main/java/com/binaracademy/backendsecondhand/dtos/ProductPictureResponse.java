package com.binaracademy.backendsecondhand.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductPictureResponse {

	private String id;
	private String name;
	private String url;
	private String contentType;
}
