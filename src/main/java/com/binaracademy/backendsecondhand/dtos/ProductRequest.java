package com.binaracademy.backendsecondhand.dtos;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {

	private String name;
	private Integer price;
	private String description;
	private List<Integer> categories;
	private Integer sellerId;
	private List<MultipartFile> documents;
}
