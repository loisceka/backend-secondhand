package com.binaracademy.backendsecondhand.dtos;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse {

	private String accessToken;
	private Set<String> authorities;
	private Integer userId;
}
