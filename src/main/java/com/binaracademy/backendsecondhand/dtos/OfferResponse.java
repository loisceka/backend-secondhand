package com.binaracademy.backendsecondhand.dtos;

import java.sql.Date;
import java.time.LocalDateTime;

import com.binaracademy.backendsecondhand.entities.User;
import com.binaracademy.backendsecondhand.utils.StatusOffer;
import com.binaracademy.backendsecondhand.utils.StatusTransaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OfferResponse {

	private Integer id;
	private Integer priceOffer;
	private ProductResponse product;
	private UserResponse buyer;
	private String dateOffer;
	private StatusOffer statusOffer;
	private StatusTransaction statusTransaction;
}
