package com.binaracademy.backendsecondhand.dtos;

import com.binaracademy.backendsecondhand.entities.location.City;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountResponse {

	private Integer id;
	private String name;
	private String address;
	private City city;
	private String phone;
	private String imageUrl;
}
