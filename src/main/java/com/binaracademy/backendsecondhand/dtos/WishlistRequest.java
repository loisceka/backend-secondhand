package com.binaracademy.backendsecondhand.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WishlistRequest {

	private Integer productId;
	private Integer buyerId;
}
