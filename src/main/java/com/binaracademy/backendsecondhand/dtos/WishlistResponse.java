package com.binaracademy.backendsecondhand.dtos;

import java.time.LocalDateTime;

import com.binaracademy.backendsecondhand.entities.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WishlistResponse {

	private Integer id;
	private String wishlistDate;
	private ProductResponse product;
	private User buyer;
	
}
