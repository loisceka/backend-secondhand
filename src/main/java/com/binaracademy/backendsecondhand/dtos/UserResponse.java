package com.binaracademy.backendsecondhand.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {

	private Integer id;
	private String email;
	private AccountResponse accountId;
}
